## Setting up GitLab Runners on GKE
Reference: [GitLab Runner Helm Chart](https://docs.gitlab.com/runner/install/kubernetes.html)

### Install helm charts
[Install Helm for your system](https://helm.sh/docs/using_helm/#install-helm)

### Installing the GitLab Runner helm chart - with Tiller
- Initialize Helm and Install Tiller in current kubectl context (`kubectl config current-context`. Assuming you are in the root directory of this repo, please run the commands below.
```
kubectl apply -f helm/helm-service-account.yaml
helm init --service-account tiller --upgrade
# Verify that the tiller-deploy pod is running
kubectl -n kube-system get po | grep tiller
```
Reference: [https://helm.sh/docs/using_helm/#initialize-helm-and-install-tiller](initialize-helm-and-install-tiller)

- Alternatively, if you have already installed helm and want to upgrade it please run the command below.
```
helm init --service-account tiller --upgrade
# Verify that the tiller-deploy pod is running
kubectl -n kube-system get po | grep tiller
```

- Please adjust [my-values.yaml](helm/my-values.yaml) file in this repo as needed.
   - Adjust `runnerRegistrationToken` from GitLab CI / CD
   - Adjust the `image` key if you built and publised your own Docker container. The default image is `kawsark/gitlab-ruby-curl:0.0.1`
   - Ensure the `tags` key has `curl` and any other `tags` in `.gitlab-ci.yml`.
```
cd helm/
kubectl apply -f ns.yaml 
#git clone git@gitlab.com:charts/gitlab-runner.git
helm repo add gitlab https://charts.gitlab.io
helm repo update

# Substitute <your-registration-token> below:
sed -e "s/my-runner-token/<your-registration-token>" < my-values-example.yaml > my-values.yaml

# Adjust my-values.yaml file further if needed
helm install --namespace gitlab --name gitlab-runner -f my-values.yaml gitlab/gitlab-runner
helm ls
```
Reference: [GitLab Runner Helm Chart](https://docs.gitlab.com/runner/install/kubernetes.html)
Note: if you are getting RBAC issues during helm deployment, please consult this excellent guide [Configure RBAC for Helm](https://docs.bitnami.com/kubernetes/how-to/configure-rbac-in-your-kubernetes-cluster/#use-case-2-enable-helm-in-your-cluster).

### Check pods deployed
```
kubectl get pods -n gitlab
kubectl logs -f <podname> -n gitlab
```

### Installing the GitLab Runner helm chart - No Tiller
These steps will allow us to install GitLab Runner without the server side Tiller admin account.
_Pending_
